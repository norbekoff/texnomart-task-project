export const store = (function createStore() {
  let state = {
	user: null
  };

  function getState() {
    return state;
  }

  function setUser(data) {
    state.user = data;
  }

  function getUser() {
    return state.user;
  }

  return { getState, setUser, getUser };
})();
