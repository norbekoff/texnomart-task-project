import React from "react";

const InputField = ({
  label,
  inputType = "text",
  // changeHandler,
  placeholder,
  hasError,
  register,
}) => {
  return (
    <div className="flex flex-col gap-1 text-sm lg:text-base">
      <label
        className="capitalize font-bold tracking-wide text-[#8c97ab]"
        htmlFor={label}>
        {label}
      </label>
      <div className="w-full">
        <input
          className="px-4 py-2 border-2 border-[#e6e7ec] w-full rounded-[4px] transition-all duration-200 outline-none focus:border-[#0062ff]"
          {...register}
          type={inputType}
          placeholder={placeholder}
          id={label}
          // onChange={(e) => changeHandler(e.target.value)}
        />
        <div
          className={[
            "text-red-400 transition-all duration-200",
            hasError ? "opacity-100" : "opacity-0",
          ]
            .filter(Boolean)
            .join(" ")}>
          <>{hasError?.message}</>
        </div>
      </div>
    </div>
  );
};

export default InputField;
