import React, { useEffect, useState } from "react";
import StarRating from "components/base/StarRating";
import { formatMoney } from "lib/globals";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import TextareaAutosize from "@mui/material/TextareaAutosize";
import { useForm, SubmitHandler } from "react-hook-form";
import Rating from "@mui/material/Rating";
import Snackbar from "@mui/material/Snackbar";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import axios from "lib/axios";

interface Props {
  item: any;
  productId: number;
}

interface IFormInput {
  text: string;
}

export default function ProductDescription({ item, productId }: Props) {
  const [open, setOpen] = useState(false);
  const [alert, setAlert] = useState(false);
  const [rating, setRating] = useState(5);
  const [toastMessage, setToastMessage] = useState("");
  const attributes = item?.mainAttributes?.slice(0, 4);
  const { register, handleSubmit } = useForm<IFormInput>();
  const [tokenExists, setTokenExists] = useState("");
  useEffect(() => {
    const token = localStorage.getItem("token") || "";
    setTokenExists(token);
  }, []);

  const handleOpen = () => {
    if (tokenExists) {
      setOpen(true);
    } else {
      setToastMessage(`Izoh qoldrish uchun ro'yhatdan o'ting.`);
      setAlert(true);
    }
  };
  const handleClose = () => setOpen(false);

  const onSubmit: SubmitHandler<IFormInput> = async (data) => {
    await axios
      .post(
        `api/application/product/add-review`,
        {
          rate: rating,
          comments: data?.text,
          product_id: productId,
        },
        {
          headers: {
            token: tokenExists,
          },
        }
      )
      .then((res) => {
        setToastMessage(`Sizning izohingi yuborildi`);
        setAlert(true);
        setOpen(false);
      })
      .catch((err) => {
        setToastMessage(`Oooops, nimadur bo'ldi 🤔`);
        setAlert(true);
      });
  };

  const handleALertClose = (
    event: React.SyntheticEvent | Event,
    reason?: string
  ) => {
    if (reason === "clickaway") {
      return;
    }

    setAlert(false);
  };

  const alertAction = (
    <React.Fragment>
      <IconButton
        size="small"
        aria-label="close"
        color="inherit"
        onClick={handleALertClose}>
        <CloseIcon fontSize="small" />
      </IconButton>
    </React.Fragment>
  );

  return (
    <div className="">
      <h6 className="font-semibold ">Mahsulot nomi: </h6>
      <p className="text-[14px] leading-[20px] mt-2">{item?.name}</p>

      <h6 className="font-semibold mt-7">Mahsulot haqida qisqacha: </h6>

      {attributes.map((item: object, index: number) => {
        return (
          <div
            key={index}
            className="flex items-center justify-between mt-5 leading-4">
            <span className="text-[#66655a]">{item?.attribute_name}</span>
            <span>{item?.text}</span>
          </div>
        );
      })}
      <div className="flex flex-col gap-2 mt-7">
        <h6 className="font-semibold">Qabul qlish turlari: </h6>
        <span className=" text-[14px]">
          - Olib ketish <br /> - Yetkazib berish
        </span>
        <span className="text-[14px]">
          Yetkazib berish muddati: 1 - 5 kungacha
        </span>
      </div>

      <div className="mt-7 flex items-center gap-4 justify-between -425:!block">
        <h6 className="font-semibold text-[25px]">
          {formatMoney(item?.sale_price)} UZS
        </h6>
        <div className="flex  flex-col items-end gap-1 -425:items-start -425:mt-7">
          <span
            onClick={handleOpen}
            className="text-[17px] hover:opacity-80 cursor-pointer px-3 py-2 bg-slate-200 rounded-md mb-2">
            Izoh qoldirish
          </span>
          <div className=" flex items-center">
            <StarRating readable rating={item?.reviewsMiddleRate} />
            <span className="ml-2">{item?.reviewsCount} ta sharh</span>
          </div>
        </div>
      </div>

      <Snackbar
        open={alert}
        autoHideDuration={6000}
        onClose={handleALertClose}
        message={toastMessage}
        anchorOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        action={alertAction}
      />

      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description">
        <Box sx={style} className="w-[600]">
          <div className="flex justify-between items-center mb-7">
            <h6 className="font-semibold ">Izoh qoldrish</h6>
            <span
              onClick={handleClose}
              className="hover:text-red-400 cursor-pointer">
              yopish
            </span>
          </div>

          <div className="flex items-center gap-5">
            <span>Sizning bahoingiz</span>
            <Rating
              name="simple-controlled"
              value={rating}
              onChange={(event, newValue) => {
                setRating(newValue);
              }}
            />
          </div>

          <form onSubmit={handleSubmit(onSubmit)} className="mt-7">
            <span>Sizning izohingiz</span>
            <TextareaAutosize
              className="w-full   p-4 mt-3"
              minRows={3}
              placeholder="Afzalliklari"
              {...register("text", { required: true })}
            />
          </form>

          <div className="flex items-end justify-end mt-7">
            <button
              type="submit"
              onClick={handleSubmit(onSubmit)}
              className="border-2 border-black px-4 py-2 ">
              Yuborish
            </button>
          </div>
        </Box>
      </Modal>
    </div>
  );
}

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: "400px",
  bgcolor: "background.paper",
  boxShadow: 24,
  border: "none",
  p: 4,
};
