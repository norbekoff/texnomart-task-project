import React, { useState } from "react";
import { Pagination, Navigation, Thumbs, FreeMode } from "swiper";

import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/thumbs";
import "swiper/css/free-mode";

const baseUrl = process.env.NEXT_PUBLIC_BASE_URL;
const swiperNavigation = {
  nextEl: ".swiper-btn-next",
  prevEl: ".swiper-btn-prev",
};

interface Props {
  images: string[];
}

export default function ProductSingle({ images }: Props) {
  const [thumbsSwiper, setThumbsSwiper] = useState(null);

  return (
    <div>
      <div>
        <Swiper
          modules={[Thumbs, FreeMode]}
          loop
          lazy
          thumbs={{ swiper: thumbsSwiper }}>
          {images?.map((item, index) => {
            return (
              <SwiperSlide className="flex-center" key={index}>
                <div className="bg-white">
                  <img
                    src={baseUrl + item}
                    className="w-[598px] h-[400px] object-contain object-center cursor-pointer"
                  />
                </div>
              </SwiperSlide>
            );
          })}
        </Swiper>
      </div>

      <div className="relative mt-10">
        <img
          className="absolute left-0 top-[50%] translate-y-[-50%] swiper-btn-prev cursor-pointer"
          src="https://texnomart.uz/_nuxt/img/thumb-arrow-left.8d9741a.svg"
        />
        <div className="px-10">
          <Swiper
            modules={[Pagination, Navigation, Thumbs, FreeMode]}
            spaceBetween={4}
            navigation={swiperNavigation}
            slidesPerView={4}
            onSlideChange={setThumbsSwiper}
            watchSlidesProgress
            freeMode
            onSwiper={setThumbsSwiper}>
            {images?.map((item, index) => {
              return (
                <SwiperSlide className="flex-center" key={index}>
                  <div className="bg-white">
                    <img
                      src={baseUrl + item}
                      className="w-[64px] cursor-pointer h-[64px] object-contain object-center rounded-[6px] border-[2px] border-transparent"
                    />
                  </div>
                </SwiperSlide>
              );
            })}
          </Swiper>
        </div>
        <img
          src="https://texnomart.uz/_nuxt/img/thumb-arrow-right.104a3f5.svg"
          className="absolute right-0 top-[50%] translate-y-[-50%] swiper-btn-next cursor-pointer"
        />
      </div>
    </div>
  );
}
