import { formatMoney } from "../../lib/globals";
import Link from "next/link";
import StarRating from "../base/StarRating";
import Image from "next/image";
export default function ProductCard({
  image = "files/global/icons/75.png",
  title = "Hello there",
  price,
  reviewsMiddleRate,
  reviewsCount,
  id,
  className,
}) {
  return (
    <Link href={"/products/" + id}>
      <div
        className={"product-card hover:shadow-2xl cursor-pointer " + className}>
        <div className="relative w-full h-[160px] mb-6 cursor-pointer">
          <Image
            alt="Image Alt"
            src={"https://dev-back.texnomart.uz/" + image}
            loading="lazy"
            layout="fill"
            objectFit="contain" // Scale your image down to fit into the container
          />
        </div>
        <Link href={"/products/" + id}>
          <p className="product-card__title line-clamp-2 cursor-pointer hover:text-blue-600 transition min-h-[47px] mb-3">
            {title}
          </p>
        </Link>
        <div className="flex items-center gap-1 mb-3">
          <StarRating rating={reviewsMiddleRate} />
          <span className="text-[14px]">{reviewsCount} ta sharh</span>
        </div>
        {/*  eslint-disable-next-line react/no-unescaped-entities */}
        <p className="font-medium text-[18px]">{formatMoney(price)} so'm</p>
      </div>
    </Link>
  );
}
