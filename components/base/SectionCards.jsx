import React, { useRef, useState } from "react";
import Link from "next/link";
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

import { Keyboard, Scrollbar, Navigation, Autoplay } from "swiper";
import ProductCard from "../base/ProductCard";

// Import Swiper styles
import "swiper/css";
import "swiper/css/scrollbar";
import "swiper/css/navigation";
import "swiper/css/pagination";

const swiperNavigation = {
  nextEl: ".swiper-btn-next",
  prevEl: ".swiper-btn-prev",
};

export default function SectionCards({ title, data, slug, className = "" }) {
  return (
    <div className={className}>
      <div className="flex-between-center px-4">
        <h3 className="text-2xl font-medium mb-2 capitalize">{title}</h3>
        <Link href={"/products/category/" + slug}>
          <a className="hover:text-blue-500">Barcha {title}</a>
        </Link>
      </div>
      <div className="">
        <Swiper
          slidesPerView={1}
          centeredSlides={false}
          slidesPerGroupSkip={1}
          spaceBetween={-10}
          keyboard={{
            enabled: true,
          }}
          breakpoints={{
            1000: {
              slidesPerView: 5,
              slidesPerGroup: 1,
            },
            800: {
              slidesPerView: 4,
              slidesPerGroup: 1,
            },
            600: {
              slidesPerView: 3,
              slidesPerGroup: 1,
            },
            400: {
              slidesPerView: 2,
              slidesPerGroup: 1,
            },
          }}
          navigation={true}
          modules={[Keyboard, Navigation, Autoplay]}
          className="mySwiper">
          {data?.map((item, key) => (
            <SwiperSlide key={key}>
              <div className="p-4 ">
                <ProductCard
                  className={"!shadow-none"}
                  key={key}
                  title={item.name}
                  price={item.sale_price}
                  image={item.image}
                  reviewsCount={item.reviewsCount}
                  reviewsMiddleRate={item.reviewsMiddleRate}
                  id={item.id}
                />
              </div>
            </SwiperSlide>
          ))}
        </Swiper>
      </div>
    </div>
  );
}
