import Link from "next/link";
import Image from "next/image";
import { useState } from "react";

export default function MenuBar({ className, catalog, setMenuIsOpen }) {
  const defaultCategory = catalog[0].subArray;
  const [currentCategories, setCurrentCategories] = useState(defaultCategory);
  const [currentActive, setCurrentActive] = useState(0);

  return (
    <div className={className}>
      <div className="container flex">
        <ul className="flex flex-col max-h-[80vh] overflow-auto scrollbar py-4 pr-4 translate-x-[-16px]">
          {catalog.map((item, index) => (
            <li
              onClick={() => setCurrentCategories(item.subArray)}
              onMouseMove={() => {
                setCurrentCategories(item.subArray);
                setCurrentActive(index);
              }}
              key={index}
              className={
                "flex items-center gap-4 cursor-pointer hover: px-4 py-2 rounded-md " +
                (index === currentActive ? "bg-neutral-100" : "")
              }>
              <Image
                src={
                  "https://dev-back.texnomart.uz/" +
                  item.category.small_front_icon
                }
                height={24}
                width={24}
                alt=""
              />
              <span className="font-medium">{item.category.title}</span>
            </li>
          ))}
        </ul>
        <div className="flex-1 max-h-[80vh] overflow-auto scrollbar py-4">
          <div className="w-full columns-4 -1180:columns-3 -950:columns-2 -700:columns-1">
            {!!currentCategories.length &&
              currentCategories.map((item, key) => {
                // if (!item.subArray.length) return;
                return (
                  <div key={key} className="mb-8">
                    <div className="font-medium mb-2 uppercase">
                      {item.category.title}
                    </div>
                    <ul className="flex flex-col gap-2">
                      {item.subArray.map((inner, key) => {
                        return (
                          <li
                            key={key}
                            className="hover:text-blue-700"
                            onClick={() => {
                              setMenuIsOpen(false);
                            }}>
                            <Link href={"/products/category/" + inner.slug}>
                              {inner.title}
                            </Link>
                          </li>
                        );
                      })}
                    </ul>
                  </div>
                );
              })}
          </div>
        </div>
      </div>
    </div>
  );
}
