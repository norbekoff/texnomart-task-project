import Link from "next/link";
import axios from "../../lib/axios";
import { store } from "../../store/index";
import { useEffect, useState } from "react";
import MenuBar from "../MenuBar";
import Burger from "../base/Burger";

function Header({}) {
  const [categories, setCategories] = useState([]);
  const [catalog, setCatalog] = useState([]);
  const [user, setUser] = useState();
  const [menuIsOpen, setMenuIsOpen] = useState(false);
  useEffect(() => {
    (async function () {
      const token = localStorage.getItem("token");
      if (token) {
        const res = await axios
          .get("api/application/profile/index", {
            headers: {
              token: token,
            },
          })
          .catch(() => {
            localStorage.removeItem("token");
          });
        const data = res?.data?.data;
        setUser(data);
        store.setUser(data);
      }
    })();

    (async function () {
      const res = await axios.get("api/application/home/categories?type=top");
      const data = await res.data.data;
      setCategories(data);
    })();

    (async function () {
      const res = await axios.get("api/application/home/popup-categories");
      const data = await res.data.data;
      setCatalog(data);
    })();
  }, []);

  function logOut() {
    localStorage.removeItem("token");
    setUser(null);
    store.setUser(null);
  }
  return (
    <header className="header z-20  fixed top-0 w-full left-0 bg-white backdrop-blur-[10px] border-b border-secondary border-opacity-[0.067]">
      <div className="border-b py-5">
        <div className="container">
          <nav className="flex justify-between items-center ">
            <div className="text-2xl font-bold text-gray-600 -600:text-xl">
              <Link href="/">BRAND NAME</Link>
            </div>
            <ul className="flex-center gap-10 -600:gap-3 !text-sm">
              {user ? (
                <>
                  <li className="text-md cursor-pointer -600:hidden">
                    {user.full_name}
                  </li>
                  <li
                    className="text-md hover-link cursor-pointer "
                    onClick={logOut}>
                    Chiqish
                  </li>
                </>
              ) : (
                <>
                  <li className="text-md hover-link cursor-pointer">
                    <Link href={"/login"}>Kirish</Link>
                  </li>
                  <li className="text-md hover-link cursor-pointer">
                    {/*  eslint-disable-next-line react/no-unescaped-entities */}
                    <Link href={"/register"}>Ro'yxatdan o'tish</Link>
                  </li>
                </>
              )}
            </ul>
          </nav>
        </div>
      </div>
      <div className="py-3 -400:py-1">
        <div className="container">
          <div className="flex-between-center">
            <div
              onClick={() => setMenuIsOpen((menuIsOpen) => !menuIsOpen)}
              className="translate-x-[-15px] flex-center-center cursor-pointer	">
              <Burger menuIsOpen={menuIsOpen} />
              {/*  eslint-disable-next-line react/no-unescaped-entities */}
              <span>Barcha bo'limlar</span>
            </div>
            <ul className="flex-between-center gap-4">
              {categories?.map((item, key) => (
                <li
                  key={key}
                  className="hover:text-blue-500 -1100:even:hidden -685:hidden"
                  onClick={() => setMenuIsOpen(false)}>
                  <Link href={"/products/category/" + item.slug}>
                    {item.title}
                  </Link>
                </li>
              ))}
            </ul>
          </div>
        </div>
        {!!catalog.length && (
          <MenuBar
            setMenuIsOpen={setMenuIsOpen}
            className={
              "fixed menubar w-full bg-white " + (menuIsOpen ? "active" : "")
            }
            catalog={catalog}
          />
        )}
      </div>
    </header>
  );
}

export default Header;
