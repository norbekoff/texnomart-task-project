import Head from "next/head";
import Header from "../components/layout/Header";

function Layout({ children, data }) {
  return (
    <>
      <Head>
        <title>Brand Name</title>
      </Head>
      <Header />
      <main className="mt-36">{children}</main>
    </>
  );
}

export default Layout;
