import "../styles/globals.scss";
import Layout from "../layouts/default";
import axios from "../lib/axios";
import { store } from "../store/index";
import { useEffect, useState } from "react";

function MyApp({ Component, pageProps, data }) {
  if (Component.layout === "blank") {
    return <Component {...pageProps} />;
  }

  // Use the layout defined at the page level, if available 
  const getLayout = Component.getLayout || ((page) => <Layout>{page}</Layout>);

  return getLayout(<Component {...pageProps} />);
}

export default MyApp;
