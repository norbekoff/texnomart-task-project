import type { NextPage } from "next";
import Loader from "../components/base/Loader.jsx";
import { useEffect, useState } from "react";
import axios from "lib/axios";
import SectionCards from "../components/base/SectionCards";

interface Posts {
  userId: number;
  id: number;
  title: string;
  body: string;
}

interface Props {
  posts: Posts[];
  categories: [];
}

const Home: NextPage<Props> = () => {
  const [tvs, setTvs] = useState([]);
  const [smartphones, setSmartphones] = useState([]);
  const [noutbooks, setNoutbooks] = useState([]);
  useEffect(() => {
    (async function () {
      const [res, res1, res2] = await Promise.all([
        axios.get(
          `api/application/product/filter-products?min-price=0&max-price=5000000&sort=-popular&stik_id=&category_slug=smartfony&region=2`
        ),
        axios.get(
          `api/application/product/filter-products?min-price=0&max-price=5000000&sort=-popular&stik_id=&category_slug=noutbuki-i-desktopy&region=2`
        ),
        axios.get(
          `api/application/product/filter-products?min-price=0&max-price=5000000&sort=-popular&stik_id=&category_slug=vse-televizory&region=2`
        ),
      ]);

      const phones = res.data.data.products.items;
      const noutbooks = res1.data.data.products.items;
      const tvs = res2.data.data.products.items;
      setSmartphones(phones);
      setNoutbooks(noutbooks);
      setTvs(tvs);
    })();
  }, []);

  if (!tvs.length)
    return (
      <div className="container">
        <Loader />
      </div>
    );
  return (
    <div className="py-10">
      <div className="container ">
        <SectionCards
          className="mb-10"
          title="smartfonlar"
          data={smartphones}
          slug={"smartfony"}
        />
        <SectionCards
          className="mb-10"
          title="shaxsiy komputerlar"
          data={noutbooks}
          slug={"noutbuki-i-desktopy"}
        />
        <SectionCards slug={"vse-televizory"} title="televizorlar" data={tvs} />
      </div>
    </div>
  );
};

export default Home;
