import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { useRouter } from "next/router";
import InputField from "../components/froms/InputField";
import axios from "axios";
import { Snackbar } from "@mui/material";
const baseURL = process.env.NEXT_PUBLIC_BASE_URL;

export default function Register() {
  const router = useRouter();
  const [phone, setPhone] = useState("");
  const [toastOpen, setToastOpen] = useState(false);
  const [severity, setToastSeverity] = useState("success");
  const [toastMessage, setToastMessage] = useState("");
  const [step, setStep] = useState("register");
  const [count, setCount] = useState(0);
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm();

  const handleRegsiter = async (formData) => {
    setPhone("+998" + formData.phone);
    const data = {
      phone: "+998" + formData.phone,
      first_name: formData.firstName,
      last_name: formData.lastName,
    };

    if (step === "register") {
      await axios
        .post(`${baseURL}api/v1/user/register`, data)
        .then((res) => {
          if (!res.data.success) {
            throw new Error(res.data.message);
          }
          setCount(res.data.data.expired_in);
          setToastMessage("Kod yuborildi");
          setToastSeverity("success");
          setStep("verify");
          reset();
        })
        .catch((err) => {
          setToastMessage(err.message);
          setToastSeverity("error");
          console.log(err, "error");
        })
        .finally(() => {
          setToastOpen(true);
        });
    } else {
      await axios
        .post(`${baseURL}api/v1/user/verify-code`, {
          phone,
          code: formData.code,
        })
        .then((res) => {
          if (!res.data.success) {
            throw new Error(res.data.message);
          }
          setToastMessage("Logged in successfully");
          setToastSeverity("success");
          localStorage.setItem("token", res.data.data.token);
          router.push("/");
        })
        .catch((err) => {
          setToastMessage(err.message);
          setToastSeverity("error");
          console.log(err, "error");
        })
        .finally(() => {
          setToastOpen(true);
        });
    }
  };

  useEffect(() => {
    let interval;

    interval = setInterval(() => {
      setCount((count) => {
        if (count === 0) {
          clearInterval(interval);
          setStep("register");
        }
        return count - 1;
      });
    }, 1000);

    return () => clearInterval(interval);
  }, [step]);

  return (
    <div className="container px-4 mx-auto">
      <Snackbar
        anchorOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        open={toastOpen}
        onClose={() => setToastOpen(false)}
        autoHideDuration={6000}
        message={toastMessage}
      />
      <form
        onSubmit={handleSubmit(handleRegsiter)}
        className="flex flex-col gap-3 max-w-lg mx-auto bg-white mt-16 px-4 py-5 rounded-md border-[1px] border-gray-200">
        <h1 className="text-3xl font-semibold mb-4">Register</h1>

        {step === "register" && (
          <>
            <InputField
              inputType="text"
              label="phone"
              placeholder="94 999 99 99"
              register={register("phone", {
                required: "Phone is requred field",
                minLength: { value: 9, message: "Phone should have 9 digits" },
                maxLength: { value: 9, message: "Phone should have 9 digits" },
              })}
              hasError={errors.phone}
            />
            <InputField
              inputType="text"
              label="first name"
              placeholder="Your name: e.g Jasurbek"
              register={register("firstName", {
                required: "First name is required",
              })}
              hasError={errors.firstName}
            />
            <InputField
              inputType="text"
              label="last name"
              placeholder="Your surname: e.g Norbekov"
              register={register("lastName", {
                required: "Last name is required",
              })}
              hasError={errors.lastName}
            />
          </>
        )}
        {step === "verify" && (
          <div className="verify">
            <p>{phone}</p>
            {count > 0 ? <p>Code expires in {count}</p> : <p>Resend</p>}
            <InputField
              inputType="text"
              label=""
              placeholder="Code"
              register={register("code", {
                required: "Enter the code please",
                minLength: { value: 5, message: "Code should have 5 digits" },
                maxLength: { value: 5, message: "Code should have 5 digits" },
              })}
              hasError={errors.phone}
            />
          </div>
        )}
        <button className="mx-auto mt-5 bg-[#0062ff] border-none rounded-[4px] text-base text-center text-white px-12 py-[10px] transition-colors duration-200 hover:bg-[#0062ffe6]">
          Submit
        </button>
      </form>
    </div>
  );
}

Register.getLayout = () => (
  <div>
    <Register />
  </div>
);
