import ProductCard from "../../../components/base/ProductCard";
import Loader from "components/base/Loader.jsx";
import { useRouter } from "next/router";
import Head from "next/head";
import axios from "../../../lib/axios";
import { useEffect, useState } from "react";
import { Pagination } from "@mui/material";


export default function ProductCategory() {
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [pageCount, setPageCount] = useState(0);
  const [page, setPage] = useState(1);

  const router = useRouter();
  const { slug } = router.query;

  useEffect(() => {
    (async function () {
      setIsLoading(true);
      const res = await axios
        .get(
          `api/application/product/filter-products?min-price=0&max-price=5000000&sort=-popular&stik_id=&category_slug=${slug}&region=2&page=${page}`
        )
        .finally(() => setIsLoading(false));
      if (!res.data.success) return;
      setData(res.data.data);
      setPage(res.data.data.products._meta.currentPage);
      setPageCount(res.data.data.products._meta.pageCount);
    })();
  }, [slug, page]);

  function handleChange(e, value) {
    setPage(value);
  }

  if (isLoading) {
    return <Loader />;
  }

  return (
    <>
      <Head>
        <title>Mahsulotlar</title>
      </Head>
      <div className="py-10">
        <div className="container">
          {data.products?.items.length ? (
            <div>
              <div className="grid grid-cols-5 gap-4 mb-8 -1050:grid-cols-4 -850:grid-cols-3 -650:grid-cols-2 -450:grid-cols-1">
                {data.products.items.map((item, key) => (
                  <ProductCard
                    key={key}
                    title={item.name}
                    price={item.sale_price}
                    image={item.image}
                    reviewsCount={item.reviewsCount}
                    reviewsMiddleRate={item.reviewsMiddleRate}
                    id={item.id}
                  />
                ))}
              </div>
              <div className="flex justify-end">
                <Pagination
                  shape="rounded"
                  size="large"
                  count={pageCount}
                  page={page}
                  onChange={handleChange}
                />
              </div>
            </div>
          ) : (
            <div className="text-center mt-32 text-2xl">Nothing found</div>
          )}
        </div>
      </div>
    </>
  );
}
