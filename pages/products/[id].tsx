import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import ProductReview from "components/page/products/ProductReview";
import ProductDescription from "components/page/products/ProductDescription";
import axios from "lib/axios";

export default function ProductSingle({ single = null, productId }) {
  return (
    <div className="py-10">
      <div className="container">
        <div className="grid grid-cols-2 gap-8  -900:grid-cols-1 -900:mb-10">
          <ProductReview images={single?.images} />
          {single && <ProductDescription item={single} productId={productId} />}
        </div>
      </div>
    </div>
  );
}

export async function getServerSideProps({ params }) {
  const { id } = params;
  const res = await axios.get(
    `api/application/product/detail-main?product_id=${id}`
  );
  const single = res.data.data.product;
  if (!res.data.success)
    return {
      notFound: true,
    };
  return {
    props: { single, productId: id },
  };
}
