import InputField from "../components/froms/InputField";
import { useRouter } from "next/router";
import { useForm } from "react-hook-form";
import axios from "axios";
import { useState } from "react";
import { Snackbar } from "@mui/material";
const baseURL = process.env.NEXT_PUBLIC_BASE_URL;

export default function Login() {
  const router = useRouter();
  const [toastOpen, setToastOpen] = useState(false);
  const [severity, setToastSeverity] = useState("success");
  const [toastMessage, setToastMessage] = useState("");
  const [phone, setPhone] = useState("");
  const [step, setStep] = useState("getCode");

  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm();

  const handleLogin = async (formData) => {
    if (step === "getCode") {
      await axios
        .post(`${baseURL}api/v2/user/login-phone`, {
          phone: "+998" + formData.phone,
        })
        .then((res) => {
          if (!res.data.success) {
            throw new Error(res.data.message);
          }
          setToastMessage("Kod yuborildi");
          setToastSeverity("success");
          setPhone("+998" + formData.phone);
          setStep("verifyCode");
        })
        .catch((err) => {
          setToastMessage(err.message);
          setToastSeverity("error");
          console.log(err);
        })
        .finally(() => {
          setToastOpen(true);
        });
    } else {
      await axios
        .post(`${baseURL}api/v2/user/login-phone-verify`, {
          phone,
          code: formData.code,
        })
        .then((res) => {
          if (!res.data.success) {
            throw new Error(res.data.message);
          }
          setToastMessage("Kod yuborildi");
          setToastSeverity("success");
          localStorage.setItem("token", res.data.data.token);
          router.push("/");
        })
        .catch((err) => {
          setToastMessage(err.message);
          setToastSeverity("error");
          console.log(err);
        })
        .finally(() => {
          setToastOpen(true);
        });
    }
  };

  return (
    <div className="container px-4 mx-auto">
      <Snackbar
        anchorOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        open={toastOpen}
        onClose={() => setToastOpen(false)}
        autoHideDuration={6000}
        message={toastMessage}
      />
      <form
        onSubmit={handleSubmit(handleLogin)}
        className="flex flex-col gap-3 max-w-lg mx-auto bg-white mt-16 px-4 py-5 rounded-md border-[1px] border-gray-200">
        <h1 className="text-3xl font-semibold mb-4">Login</h1>
        {step === "getCode" && (
          <InputField
            key={1}
            inputType="text"
            label="phone"
            placeholder="94 999 99 99"
            register={register("phone", {
              required: "Phone is requred field",
              minLength: { value: 9, message: "Phone should have 9 digits" },
              maxLength: { value: 9, message: "Phone should have 9 digits" },
            })}
            hasError={errors.phone}
          />
        )}

        {step === "verifyCode" && (
          <InputField
            key={3}
            inputType="number"
            label="code"
            placeholder="code"
            register={register("code", {
              required: "Code is requred field",
              minLength: { value: 5, message: "Code should have 5 digits" },
              maxLength: { value: 5, message: "Code should have 5 digits" },
            })}
            hasError={errors.code}
          />
        )}

        <button className="mx-auto mt-5 bg-[#0062ff] border-none rounded-[4px] text-base text-center text-white px-12 py-[10px] transition-colors duration-200 hover:bg-[#0062ffe6]">
          Login
        </button>
      </form>
    </div>
  );
}

Login.getLayout = () => (
  <div>
    <Login />
  </div>
);
